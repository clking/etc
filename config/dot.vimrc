source $VIMRUNTIME/vimrc_example.vim

set encoding=utf-8
set fileencoding=utf-8
set termencoding=utf-8
set shiftwidth=4
set tabstop=8
set softtabstop=8
set cindent
set foldmethod=marker
set et
set mouse=
set nrformats=hex
set cursorline
set path=,,..
set bg=dark

if $TERM =~ '^screen'
    " set title
    set t_ts=k
    set t_fs=\
    auto BufEnter,BufRead,BufWinEnter,WinEnter * :set title | let &titlestring = '[V]' . expand('%:t')

    " rely on tmux automatic-rename instead to restore window title
    " | let &titleold = strpart($SHELL, strridx($SHELL, '/') + 1)
    "auto VimLeave * :set title | let &titlestring = &titleold
endif

syn on

au BufNewFile,BufRead,TabEnter *.wpm setf wpm
au BufNewFile,BufRead,TabEnter *.c,*.cpp,*.java set cin
au BufNewFile,BufRead,TabEnter *.pl,*.pm set ai
au BufNewFile,BufRead,TabEnter *.c,*.cpp,*.java,*.pl,*.pm set path=,,..
au BufNewFile,BufRead,TabEnter *.css set ai nocin
au BufNewFile,BufRead,TabEnter *.log,*.txt set nowrap spell
au BufNewfile,BufRead,TabEnter svn-commit*,svn-prop,COMMIT_EDITMSG*,git-rebase-todo set nobackup viminfo=
au BufNewfile,BufRead,TabEnter *patch set nofoldenable
au BufNewfile,BufRead,TabEnter *.html set sw=2

au BufLeave,TabLeave * if maparg("<C-C>") | unmap <C-C> | endif
au BufLeave,TabLeave * if maparg("<C-C>", 'i') | iunmap <C-C> | endif
au BufLeave,TabLeave * set colorcolumn=

au BufNewfile,BufRead,TabEnter *.pl map <C-C> :w<CR>:!perl -c %<CR>
au BufNewfile,BufRead,TabEnter *.pl imap <C-C> <ESC>:w<CR>:!perl -c %<CR>
au BufNewfile,BufRead,TabEnter *.pm map <C-C> :w<CR>:!perl -I. -c %<CR>
au BufNewfile,BufRead,TabEnter *.pm imap <C-C> <ESC>:w<CR>:!perl -I. -c %<CR>
au BufNewfile,BufRead,TabEnter *.php map <C-C> :w<CR>:!php -l %<CR>
au BufNewfile,BufRead,TabEnter *.php map <C-C> <ESC>:w<CR>:!php -l %<CR>
au BufNewfile,BufRead,TabEnter *.sh map <C-C> :w<CR>:!sh -n %<CR>
au BufNewfile,BufRead,TabEnter *.sh imap <C-C> <Esc>:w<CR>:!sh -n %<CR>
au BufNewfile,BufRead,TabEnter *.py map <C-C> :w<CR>:!python3 -m py_compile %<CR>
au BufNewfile,BufRead,TabEnter *.py imap <C-C> <ESC>:w<CR>:!python3 -m py_compile %<CR>

au BufNewfile,BufRead,TabEnter *.pl,*.pm,*.php,*.js,*.py,*.c,*.java set colorcolumn=120
au BufWritePre *.pl,*.pm,*.php,*.js,*.py,*.c,*.java*.css :%s/\s\+$//e

" highlight trailing whitespaces and spaces before a tab
au BufNewFile,BufRead,TabEnter * hi ExtraWhitespace ctermbg=red guibg=red
au BufNewFile,BufRead,TabEnter * match ExtraWhitespace /\s\+$\| \+\ze\t/

" utility key mappings
map g1 :tabn 1<CR>
map g2 :tabn 2<CR>
map g3 :tabn 3<CR>
map g4 :tabn 4<CR>
map g5 :tabn 5<CR>
map g6 :tabn 6<CR>
map g7 :tabn 7<CR>
map g8 :tabn 8<CR>
map g9 :tabn 9<CR>
map g0 :tabn 10<CR>
map gm1 :tabm 0<CR>
map gm2 :tabm 1<CR>
map gm3 :tabm 2<CR>
map gm4 :tabm 3<CR>
map gm5 :tabm 4<CR>
map gm6 :tabm 5<CR>
map gm7 :tabm 6<CR>
map gm8 :tabm 7<CR>
map gm9 :tabm 8<CR>
map gm0 :tabm 9<CR>

map <C-j> :tabn<CR>
map <C-k> :tabp<CR>
map <C-n> :tabe<CR>:n<CR>

imap <C-j> <Esc>:tabn<CR>
imap <C-k> <Esc>:tabp<CR>

map <Char-0x7f> <C-h>

highlight TabLineSel term=bold,underline cterm=bold,underline ctermfg=7 ctermbg=0
highlight TabLine    term=bold cterm=bold
highlight clear TabLineFill

command! Trim %s/\s\+$

let vimrc_local = expand('$HOME/.vimrc.local')
let fe = findfile(vimrc_local)
if strlen(fe) > 0
    exec 'source ' . vimrc_local
endif
