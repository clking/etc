#!/bin/sh

reorder()
{
    if [ -d $HOME/.zsh ]
    then
        for f in $HOME/.zsh/[0-9][0-9].*
        do
            [ -L $f ] && /bin/rm -fv $f
        done
    else
        /bin/mkdir $HOME/.zsh
    fi

    cd $HOME/.zsh
    i=0
    for file in $source/shell/dot.zsh/*.zsh
    do
        index=`/usr/bin/printf %02d $i`
        base=`/usr/bin/basename $file`
        /bin/ln -sv $file $index.$base
        i=$((1+i))
    done
    /bin/touch local.zsh

    cd -
}

base=$(basename $0 .sh)
if [ "$base" = "reorder-zsh" ]
then
    source=$HOME/src/gitlab/etc
    reorder $source
fi
