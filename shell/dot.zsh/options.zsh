setopt dvorak
setopt chase_links

HIST_IGNORE_SPACE=1
HIST_NO_STORE=1
HIST_NO_FUNCTIONS=1

unsetopt clobber
unsetopt append_create
