## Getting Started

### Basic installation

```shell
sh -c "$(curl -fsSL https://gitlab.com/clking/etc/-/raw/master/shell/install.sh)"
```

or

```shell
wget https://gitlab.com/clking/etc/-/raw/master/shell/install.sh
sh install.sh
```
