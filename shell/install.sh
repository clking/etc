#!/bin/sh

id=`/usr/bin/id -u`
if [ 0 -eq "$id" ]
then
    echo should not run as root
    exit 1
fi

umask 22

/usr/bin/sudo /usr/bin/apt update
/usr/bin/sudo /usr/bin/apt install -y vim-nox tmux git zsh
rehash
/usr/bin/sudo /usr/bin/chsh -s /usr/bin/zsh clk

git=`which git`
if [ ! -d $HOME/src/gitlab ]
then
    /bin/mkdir -p $HOME/src/gitlab
    cd $HOME/src/gitlab
    $git clone https://gitlab.com/clking/etc.git
fi
source=$HOME/src/gitlab/etc

cd $HOME
for f in .zshrc .make.conf .zshenv oh-my-zsh
do
    [ -h $f ] && /bin/rm -fv $f
done

if [ ! -d .oh-my-zsh ]
then
    wget=`which wget`
    curl=`which curl`

    if [ -n "$curl" ]
    then
        /bin/sh -c "$($curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" "" --unattended
    elif [ -n "$wget" ]
    then
        /bin/sh -c "$($wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" "" --unattended
    fi
fi

/bin/rm -fv .zshrc
/bin/ln -sv $source/shell/dot.make.conf .make.conf
/bin/ln -sv $source/shell/dot.zshenv .zshenv
/bin/ln -sv $source/shell/dot.zshrc .zshrc
/bin/ln -sv $HOME/.oh-my-zsh oh-my-zsh

. $source/shell/reorder-zsh.sh
reorder $source

cd $HOME
sed=`which sed`
for file in $source/config/*
do
    case $file in
        *dot.gitconfig)
            # echo git config will be processed later
            break
            ;;
        *~)
            # echo ignoring vim auto backup file $file
            break
            ;;
    esac

    base=`/usr/bin/basename $file | $sed 's/dot//'`

    if [ -h $base ]
    then
        /bin/rm -fv $base
    fi

    if [ ! -e $base ]
    then
        /bin/ln -sv $file $base
    fi
done

if [ ! -e $HOME/.ssh/id_rsa ]
then
    echo generating ssh key pair
    /bin/ssh-keygen -t rsa -b 2048 -N '' -f $HOME/.ssh/id_rsa
    /bin/cat $HOME/.ssh/id_rsa.pub
fi

if [ ! -e .gitconfig ]
then
    /bin/cp -v $source/config/dot.gitconfig .gitconfig
    $git config --global http.cookiefile $HOME/.gitcookies

    echo -n git username:' '
    read username
    $git config --global user.name "$username"
    echo -n git email:' '
    read email
    $git config --global user.email "$email"
fi
